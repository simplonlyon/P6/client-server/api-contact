<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Message;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/api/message", name="api_message")
 */
class ApiMessageController extends Controller
{
    private $serializer;

    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }
    /**
     * @Route("/", methods="GET")
     */
    public function findAll() {
        $repo = $this->getDoctrine()->getRepository(Message::class);
        $messages = $repo->findAll();
        $json = $this->serializer->serialize($messages, "json");

        return JsonResponse::fromJsonString($json);
    }
    /**
     * @Route("/{id}", methods="GET")
     */
    public function findOne(Message $message) {
        
        $json = $this->serializer->serialize($message, "json");

        return JsonResponse::fromJsonString($json);
    }

    /**
     * @Route("/{id}", methods="DELETE")
     */
    public function delete(Message $message) {
        
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($message);
        $manager->flush();

        return new JsonResponse([], 204);
    }

    /**
     * @Route("/", methods="POST")
     */
    public function add(Request $req) {
        $manager = $this->getDoctrine()->getManager();

        $message = $this->serializer->deserialize($req->getContent(),
                                                    Message::class, 
                                                    "json");
        $manager->persist($message);
        $manager->flush();

        $json = $this->serializer->serialize($message, "json");

        return JsonResponse::fromJsonString($json, 201);
    }
    /**
     * @Route("/{id}", methods="PUT")
     */
    public function update(Message $message, Request $req) {
        $manager = $this->getDoctrine()->getManager();

        $body = $this->serializer->deserialize($req->getContent(),
                                                    Message::class, 
                                                    "json");
        $message->setContent($body->getContent());                                                    
        $message->setDate($body->getDate());                                                    
        $message->setSender($body->getSender());

        $manager->flush();

        $json = $this->serializer->serialize($message, "json");

        return JsonResponse::fromJsonString($json);
    }
}
