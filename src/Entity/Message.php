<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sender;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date;

    public function getId()
    {
        return $this->id;
    }

    public function getContent() : ?string
    {
        return $this->content;
    }

    public function setContent(string $content) : self
    {
        $this->content = $content;

        return $this;
    }

    public function getSender() : ?string
    {
        return $this->sender;
    }

    public function setSender(string $sender) : self
    {
        $this->sender = $sender;

        return $this;
    }

    public function getDate() : ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate($date) : self
    {
        if ($date instanceof \DateTime) {
            $this->date = $date;
        } elseif(is_string($date)) {
            $this->date = \DateTime::createFromFormat("d/m/Y", $date);
            if(!$this->date) {
                $this->date = new \DateTime($date);
            }
        } elseif(is_int($date)) {
            $this->date = new \DateTime();
            $this->date->setTimestamp($date / 1000);
        }
        if(!$this->date instanceof \DateTime) {
            throw new \InvalidArgumentException("Date must be d/m/Y or timestamp or DateTime");
        }

        return $this;
    }
}
